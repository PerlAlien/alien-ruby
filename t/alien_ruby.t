use Test2::V0;
use Test::Alien;
use Test::Alien::Diag;
use Alien::Ruby;

alien_diag 'Alien::Ruby';
alien_ok 'Alien::Ruby';

# Make sure our Ruby can run at all
run_ok(['ruby', '--version'])
  ->success
  ->out_like(qr/ruby [\d\.]+/)
  ->note;

{
# Make sure our Ruby can load and run a built-in library (English)
run_ok(['ruby', '-rEnglish', '-e', 'puts $ARGV.first', 'foo'])
  ->success
  ->out_like(qr/foo/)
  ->note;
}

done_testing;
